import React from 'react'
import { add, sub, div, mul } from "./Calculator"
import './App.css';
function App() {
  return (
    <div >
      <ul className="App">
        <li>
          Addition of 40 & 4 = {add(40, 4)}
        </li>
        <li>
          Subtraction of 33 & 3 = {sub(33, 3)}
        </li>
        <li>
          division of 40 & 3 = {div(40, 3)}
        </li>
        <li>
          multiple of 33 & 3 = {mul(33, 3)}
        </li>
      </ul>
    </div>
  );
}
export default App;